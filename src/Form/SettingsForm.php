<?php

namespace Drupal\apple_pay_verification\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Configure Apple Pay Verification settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apple_pay_verification_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['apple_pay_verification.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['verification_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Verification File'),
      '#upload_location' => 'private://applepay',
      '#upload_validators' => [
        'FileExtension' => [],
      ],
      '#default_value' => $this->config('apple_pay_verification.settings')->get('verification_file'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_file = $form_state->getValue('verification_file', 0);
    if (!empty($form_file[0])) {
      $file = File::load($form_file[0]);
      $file->setPermanent();
      $file->save();
    }
    $this->config('apple_pay_verification.settings')
      ->set('verification_file', $form_state->getValue('verification_file'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
