#!/usr/bin/env bash

vendor/bin/drush si --db-url=mysql://drupal:drupal@db:3306/drupal -y
mkdir -p /var/www/html/sites/simpletest/browser_output && chmod -R 777 /var/www/html/sites/simpletest
php web/core/scripts/run-tests.sh --module apple_pay_verification --sqlite /tmp/test.sqlite --verbose