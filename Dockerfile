FROM drupal:10-apache

RUN composer require --dev drush/drush drupal/core-dev --update-with-all-dependencies
RUN apt-get update -y && apt-get install default-mysql-client -y